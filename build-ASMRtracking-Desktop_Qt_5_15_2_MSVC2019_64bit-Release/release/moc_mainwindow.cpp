/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../ASMRtracking/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[24];
    char stringdata0[530];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 21), // "on_btnCAMfind_clicked"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 24), // "on_btnCAMpreview_clicked"
QT_MOC_LITERAL(4, 59, 20), // "on_btnCAMadd_clicked"
QT_MOC_LITERAL(5, 80, 23), // "on_btnCAMremove_clicked"
QT_MOC_LITERAL(6, 104, 26), // "on_listCAMERAS_itemClicked"
QT_MOC_LITERAL(7, 131, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(8, 148, 4), // "item"
QT_MOC_LITERAL(9, 153, 28), // "on_sliderHUEmin_valueChanged"
QT_MOC_LITERAL(10, 182, 5), // "value"
QT_MOC_LITERAL(11, 188, 28), // "on_sliderHUEmax_valueChanged"
QT_MOC_LITERAL(12, 217, 28), // "on_sliderSATmin_valueChanged"
QT_MOC_LITERAL(13, 246, 28), // "on_sliderSATmax_valueChanged"
QT_MOC_LITERAL(14, 275, 28), // "on_sliderVALmin_valueChanged"
QT_MOC_LITERAL(15, 304, 28), // "on_sliderVALmax_valueChanged"
QT_MOC_LITERAL(16, 333, 23), // "on_btnAddColour_clicked"
QT_MOC_LITERAL(17, 357, 22), // "on_btnRmColour_clicked"
QT_MOC_LITERAL(18, 380, 24), // "on_btnEditColour_clicked"
QT_MOC_LITERAL(19, 405, 26), // "on_btnCOLOURpicker_clicked"
QT_MOC_LITERAL(20, 432, 24), // "on_btnHSVpreview_clicked"
QT_MOC_LITERAL(21, 457, 25), // "on_listCOLOUR_itemClicked"
QT_MOC_LITERAL(22, 483, 23), // "on_btnCAMchroma_clicked"
QT_MOC_LITERAL(23, 507, 22) // "on_btnTRACKING_clicked"

    },
    "MainWindow\0on_btnCAMfind_clicked\0\0"
    "on_btnCAMpreview_clicked\0on_btnCAMadd_clicked\0"
    "on_btnCAMremove_clicked\0"
    "on_listCAMERAS_itemClicked\0QListWidgetItem*\0"
    "item\0on_sliderHUEmin_valueChanged\0"
    "value\0on_sliderHUEmax_valueChanged\0"
    "on_sliderSATmin_valueChanged\0"
    "on_sliderSATmax_valueChanged\0"
    "on_sliderVALmin_valueChanged\0"
    "on_sliderVALmax_valueChanged\0"
    "on_btnAddColour_clicked\0on_btnRmColour_clicked\0"
    "on_btnEditColour_clicked\0"
    "on_btnCOLOURpicker_clicked\0"
    "on_btnHSVpreview_clicked\0"
    "on_listCOLOUR_itemClicked\0"
    "on_btnCAMchroma_clicked\0on_btnTRACKING_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  109,    2, 0x08 /* Private */,
       3,    0,  110,    2, 0x08 /* Private */,
       4,    0,  111,    2, 0x08 /* Private */,
       5,    0,  112,    2, 0x08 /* Private */,
       6,    1,  113,    2, 0x08 /* Private */,
       9,    1,  116,    2, 0x08 /* Private */,
      11,    1,  119,    2, 0x08 /* Private */,
      12,    1,  122,    2, 0x08 /* Private */,
      13,    1,  125,    2, 0x08 /* Private */,
      14,    1,  128,    2, 0x08 /* Private */,
      15,    1,  131,    2, 0x08 /* Private */,
      16,    0,  134,    2, 0x08 /* Private */,
      17,    0,  135,    2, 0x08 /* Private */,
      18,    0,  136,    2, 0x08 /* Private */,
      19,    0,  137,    2, 0x08 /* Private */,
      20,    0,  138,    2, 0x08 /* Private */,
      21,    1,  139,    2, 0x08 /* Private */,
      22,    0,  142,    2, 0x08 /* Private */,
      23,    0,  143,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void, QMetaType::Int,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_btnCAMfind_clicked(); break;
        case 1: _t->on_btnCAMpreview_clicked(); break;
        case 2: _t->on_btnCAMadd_clicked(); break;
        case 3: _t->on_btnCAMremove_clicked(); break;
        case 4: _t->on_listCAMERAS_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 5: _t->on_sliderHUEmin_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->on_sliderHUEmax_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->on_sliderSATmin_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->on_sliderSATmax_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->on_sliderVALmin_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->on_sliderVALmax_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->on_btnAddColour_clicked(); break;
        case 12: _t->on_btnRmColour_clicked(); break;
        case 13: _t->on_btnEditColour_clicked(); break;
        case 14: _t->on_btnCOLOURpicker_clicked(); break;
        case 15: _t->on_btnHSVpreview_clicked(); break;
        case 16: _t->on_listCOLOUR_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 17: _t->on_btnCAMchroma_clicked(); break;
        case 18: _t->on_btnTRACKING_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
